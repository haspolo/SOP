package com.gitee.sop.adminserver.bean;

import lombok.Data;

/**
 * @author tanghc
 */
@Data
public class EurekaApps {
    private EurekaApplications applications;
}
